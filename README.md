BBB Benchmark Tool
==================

To run this:

* copy server.config.json.example to server.config.json and adjust it
* start botserver.py
* Use API Mate and create a meeting "Benchmark" on the BBB-Server
* on the bot fleet:
  * configure `client.config.json` to point to the botserver URL
  * run `bot_user_client.py`
* adapt `browser_user_client.py` to the scenario you want to test
* run one instance of `browser_user_client`

To run on Debian:

~~~
apt-get install python3-websockets python3-selenium chromium-driver
~~~
