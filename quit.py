#!/usr/bin/env python

import asyncio
import json
import websockets

async def program(websocket, fut):
        await websocket.send(json.dumps({'action': 'start_browser', 'wait_time': 0.1}))
        await asyncio.sleep(15)
        await websocket.send(json.dumps({'action': 'open_url', 'url': 'https://chemnitzer.linux-tage.de/'}))
        await websocket.send(json.dumps({'action': 'click_link', 'css': '.menu_programm', 'wait_time': 0.5}))
        #await websocket.send(json.dumps({'action': 'click_link', 'text': 'Vorträge', 'wait_time': 0.5}))
        await websocket.send(json.dumps({'action': 'click_link', 'text': 'Lectures', 'wait_time': 0.5}))
        await websocket.send(json.dumps({'action': 'click_link', 'text': 'Eröffnung', 'wait_time': 2}))
        await websocket.send(json.dumps({'action': 'click_link', 'css': '.menu_programm', 'wait_time': 0.5}))
        #await websocket.send(json.dumps({'action': 'click_link', 'text': 'Vorträge', 'wait_time': 0.5}))
        await websocket.send(json.dumps({'action': 'click_link', 'text': 'Lectures', 'wait_time': 0.5}))
        await websocket.send(json.dumps({'action': 'click_link', 'text': 'Eröffnung', 'wait_time': 2}))
        await websocket.send(json.dumps({'action': 'click_link', 'css': '.menu_programm', 'wait_time': 0.5}))
        #await websocket.send(json.dumps({'action': 'click_link', 'text': 'Vorträge', 'wait_time': 0.5}))
        await websocket.send(json.dumps({'action': 'click_link', 'text': 'Lectures', 'wait_time': 0.5}))
        await websocket.send(json.dumps({'action': 'click_link', 'text': 'Eröffnung', 'wait_time': 2}))
        print("done")
        #await websocket.send(json.dumps({'action': 'chat', 'message': f'I\'m done and will wait.', 'wait_time': 0}))
        #await websocket.send(json.dumps({'action': 'wait', 'wait_time': 175}))
        #await asyncio.sleep(500)
        await websocket.send(json.dumps({'action': 'teardown', 'wait_time': 0.5}))
        #await websocket.send(json.dumps({'action': 'quit', }))
        print("program done")
        fut.set_result(True)

async def run(uri, minbots):
    fut = None
    program_run = False
    async with websockets.connect(uri) as websocket:
        await websocket.send(json.dumps({'action': 'type', 'type': 'user'}))
        await websocket.send(json.dumps({'action': 'quit'}))
        async for message in websocket:
            print("received: %s" % message)

if __name__ == '__main__':
    with open("client.config.json", "rb") as f:
        config = json.loads(f.read())
        wait_for_bots = config.get('wait_for_bots', 1)
        asyncio.get_event_loop().run_until_complete(run(config['uri'], wait_for_bots))
