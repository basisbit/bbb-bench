from selenium import webdriver
from selenium.webdriver.common.keys import Keys  
from selenium.webdriver.chrome.options import Options  
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import logging
import time

class BbbClient(object):
    def __init__(self):
        self.browser = None
    
    def start_browser(self):
        """
        starts browser
        """
        if self.browser:
            return
        options = Options()
        options.add_argument('--headless')
        options.add_experimental_option("excludeSwitches", ['enable-automation'])
        options.add_argument('--shm-size=1gb')
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument('--no-user-gesture-required')
        options.add_argument('--use-fake-ui-for-media-stream')
        options.add_argument('--use-fake-device-for-media-stream')
        options.add_argument('--lang=en_US,en')
        options.add_argument('--autoplay-policy=no-user-gesture-required')
        options.add_argument('--temp-profile')
        options.headless = True

        #set browser log
        dc = DesiredCapabilities.CHROME
        dc['goog:loggingPrefs'] = { 'browser':'ALL' }

        logging.info('Starting browser!!')
        self.browser = webdriver.Chrome(executable_path='chromedriver', options=options, desired_capabilities=dc)
        self.browser.set_window_size(1280, 1080)

    def stop_browser(self):
        """
        terminates browser
        """
        if self.browser:
            self.browser.quit()
            self.browser = None

    def screenshot(self, filename):
        if self.browser:
            self.browser.get_screenshot_as_file(filename)

    def _wait_for(self, locator, timeout=60):
        element = EC.visibility_of_element_located(locator)
        WebDriverWait(self.browser, timeout).until(element)

    def _wait_for_invisibility(self, locator, timeout=60):
        element = EC.invisibility_of_element_located(locator)
        WebDriverWait(self.browser, timeout).until(element)

    def _click(self, locator):
        self.browser.find_element(locator[0], locator[1]).click()

    def _wait_and_click(self, locator, timeout=60):
        self._wait_for(locator, timeout)
        self._click(locator)

    def type_text(self, locator, message):
        element = self.browser.find_element(locator[0], locator[1])
        element.send_keys(message)

    def wait_and_click_link(self, text, timeout=60):
        locator = (By.LINK_TEXT, text)
        self._wait_and_click(locator, timeout)

    def open_url(self, url):
        self.browser.get(url)

    def join_bbb(self, join_url):
        """
        opens bbb in browser
        """
        logging.info('Open BBB and hide elements!!')
        logging.info(join_url)
        self.browser.get(join_url)

        logging.info("browser got URL")

    def audio_mic(self):
        logging.info("waiting for overlay")
        self._wait_for((By.CSS_SELECTOR, '.ReactModal__Overlay'))
        self._click((By.CSS_SELECTOR, '[aria-label="Microphone"]'))
        self._wait_and_click((By.CSS_SELECTOR, '[aria-label="Echo is audible"'))
        self._wait_for_invisibility((By.CSS_SELECTOR, '.ReactModal__Overlay'))

    def audio_listen(self):
        logging.info("waiting for overlay")
        self._wait_for((By.CSS_SELECTOR, '.ReactModal__Overlay'), 180)
        self._click((By.CSS_SELECTOR, '[aria-label="Listen only"]'))
        self._wait_for_invisibility((By.CSS_SELECTOR, '.ReactModal__Overlay'))

    def mute(self):
        self._click((By.CSS_SELECTOR, '[aria-label="Mute"]'))

    def unmute(self):
        self._click((By.CSS_SELECTOR, '[aria-label="Unmute"]'))

    def start_camera(self):
        self._wait_and_click((By.CSS_SELECTOR, '[aria-label="Share webcam"]'))
        self._wait_and_click((By.CSS_SELECTOR, '[aria-label="Start sharing"][aria-disabled="false"]'))
        self._wait_for_invisibility((By.CSS_SELECTOR, '.ReactModal__Overlay'))

    def stop_camera(self):
        self._wait_and_click((By.CSS_SELECTOR, '[aria-label="Stop sharing webcam"]'))

    def chat(self, message):
        self._wait_for((By.CSS_SELECTOR, '#message-input'))
        logging.info("start typing message")
        self.browser.find_element_by_id('message-input').send_keys(message)
        self._click((By.CSS_SELECTOR, '[aria-label="Send message"]'))

    def take_presenter(self):
        self._click((By.CSS_SELECTOR, '[aria-label="Actions"]'))
        self._wait_and_click((By.XPATH, "//span[text()='Take presenter']"))

    def raise_hand(self):
        self._click((By.CSS_SELECTOR, '[aria-label="Raise hand"]'))

    def console_log(self):
        return self.browser.get_log('browser')


if __name__ == '__main__':
    c = BbbClient()
    c.start_browser()
    join_url = "some_join_link_generated_from_api_mate_or_something_else"
    c.join_bbb(join_url)
    c.audio_mic()
    c.mute()
    #c.audio_listen()
    c.start_camera()
    c.chat("Some noise")
    c.take_presenter()
    c.chat("More noise")
    time.sleep(30)
    c.stop_browser()
